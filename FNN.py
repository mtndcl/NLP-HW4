from settings import *
import json
import collections

import dynet as dy
import numpy as np
import random
from plot import draw_result
class ReadData:

    def __init__(self, train_data_size=False, tokenizer= False):
        with open(DATA_PATH, 'r') as myfile:
            data = myfile.read()
        if train_data_size:
            self.myData = json.loads(data)[0:train_data_size]
        else:
            self.myData = json.loads(data)
        self.train_dict = self.tokenizer(tokenizer)
        self.all_words = self.vocabulary()
        self.one_hot_indexes, self.word_from_index = self.one_hot_encoder()
        self.train_data = self.bigram_word2vec()

    def tokenizer(self, tokinezer):
        train_dict = dict()
        for i in self.myData:
            if tokinezer:
                train_dict[i["id"]] = self.poem_replacer(i["poem"])
            else:
                train_dict[i["id"]] = i["poem"]
        return train_dict

    def poem_replacer(self,poem):

        poem = poem.replace('\n', NEWLINE_TOKEN)
        poem = poem.lower()
        poem = poem + END_TOKEN
        return poem

    def vocabulary(self):
        all_words = list()
        for i in self.train_dict.values():
            all_words.extend(i.split())
        return all_words

    def one_hot_encoder(self):
        one_hot_indexes = collections.Counter(self.all_words)
        k = 0
        word_from_index = {}
        for i in one_hot_indexes:
            one_hot_indexes[i] = k
            word_from_index[k] = i
            k += 1
        return one_hot_indexes, word_from_index

    def bigram_word2vec(self):
        id_dict = dict()
        for sentence_id , poem in self.train_dict.items():
            id_list = list()
            sentence = poem.split(" ")
            for i, j in zip(sentence[0::1], sentence[1::1]):
                id_list.append([self.one_hot_indexes[i], self.one_hot_indexes[j]])
            id_dict[sentence_id] = id_list
        return id_dict


class FNN:

    def __init__(self, TRAIN_DATA ):

        import dynet_config
        # Declare GPU as the default device type
        dynet_config.set_gpu()
        # Initialize dynet import using above configuration in the current scope
        self.model = dy.Model()
        self.trainer = dy.SimpleSGDTrainer(self.model)
        self.vocab_size = TRAIN_DATA.one_hot_indexes.__len__()
        self.train_words = TRAIN_DATA.all_words
        self.train_data = TRAIN_DATA.train_data
        self.one_hot_indexes= TRAIN_DATA.one_hot_indexes
        self.word_from_index = TRAIN_DATA.word_from_index
        self.createLayers()
        self.generated = list()


    def createLayers(self):
        self.input_layer = self.model.add_lookup_parameters((self.vocab_size, EMBED_SIZE))
        self.embed_weights = self.model.add_parameters((EMBED_SIZE, EMBED_SIZE))
        self.embed_bias = self.model.add_parameters((EMBED_SIZE))
        self.output_weights = self.model.add_parameters((self.vocab_size, EMBED_SIZE))
        self.output_bias = self.model.add_parameters((self.vocab_size))
    def softmax_layer(self,word):
        dy.renew_cg()
        h1 = dy.lookup(self.input_layer, word)
        h2 = dy.tanh(dy.parameter(self.embed_weights) * h1 + dy.parameter(self.embed_bias))
        weight_softmax = dy.parameter(self.output_weights)
        bias_softmax = dy.parameter(self.output_bias)
        result = weight_softmax * h2 + bias_softmax
        return result

    def getRandomWord(self):
        word = random.choice(self.train_words)
        if word != "</n>" and word != "<s>" and word != "</s>":
            return word
        else:
            return self.getRandomWord()

    def train(self , epoch_size):
        loss_list = np.zeros(epoch_size)
        for epoch in range(epoch_size):
            loss = 0
            for j in self.train_data:
                for i in self.train_data[j]:
                    result = dy.pickneglogsoftmax(self.softmax_layer(i[0]),i[1])
                    loss += result.value()
                    result.backward()
                    self.trainer.update()
            loss_list[epoch] = loss
            print("Epoch",epoch ,"Loss :" ,loss)
        draw_result(range(epoch_size),loss_list,"Dynet-SGD-Trained-with-"+str(len(self.train_data))+"-poem-"+str(epoch_size)+"-epoch")


    def generator(self,word,number_of_lines = 5):
        input  = word
        generated_poem = list()
        generated_poem.append(input)
        total_line = number_of_lines
        scores = self.softmax_layer(self.one_hot_indexes[input]).npvalue()
        predicted_label = np.argmax(scores)
        probabilty_of_prediction = scores[predicted_label]
        total_prob = probabilty_of_prediction
        prediction_size = 1
        while(number_of_lines):

            scores = self.softmax_layer(self.one_hot_indexes[input]).npvalue()
            predicted_label = np.argmax(scores)
            probabilty_of_prediction = scores[predicted_label]
            total_prob += probabilty_of_prediction
            predicted = self.word_from_index[predicted_label]
            generated_poem.append(predicted)
            prediction_size += 1
            if predicted == "</n>" or "<s>" or "</s>":
                if predicted == "</s>":
                    break
                if predicted == "</n>":
                    number_of_lines -= 1
                if number_of_lines != 0:
                    input = self.getRandomWord()
                    generated_poem.append(input)
            else:
                input = predicted

        json = {
            "perplexity": self.perplexity(total_prob,prediction_size),
            "start word" : word,
            "wanted line number": total_line,
            "number of line produced" : total_line- number_of_lines,
            "generated poem" :' '.join(generated_poem).replace(" </n>", "\n")
        }

        self.generated.append(json)

    def perplexity(self, number, count):
        return np.power(2, (number / count))

    def get_generated_poems(self):

        for i in self.generated:
            with open("generated/"+str(i["start word"]) + '.json', 'w') as outfile:
                json.dump(i, outfile)
        return self.generated