import numpy as np
import matplotlib.pyplot as plt

def draw_result(lst_iter, lst_loss, title):
    plt.plot(lst_iter, lst_loss, '-b', label='loss')

    plt.xlabel("Epoch")
    plt.legend(loc='upper right')
    plt.title(title)
    # save image
    plt.savefig("loss/"+title+".png")  # should before show method
